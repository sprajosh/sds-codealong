---
title: Getting Started with Django
subtitle: Configuring the development environment
date: 2020-03-18
tags: ["django","python", "web development", "backend framework", "backend"]
---


<a id="org11e18f2"></a>


# Lets get Started :

- We will build a simple Web Project in Django.
- You will get a clear picture about how the things works in the case web development using Django.
- Through out the tutorials we are trying to follow professional way of development.
- We need Curiosity and Patience to get started.
- Also A little bit knowledge about programming in python (Classes, objects, functions, basics).
- You can also follow python tutorials from SDS CodeAlong, in case you feel trouble in development.
- Keep asking questions , it is important ! Follow the [FAQ's](#faq)



The following part will answer some basic questions,
If you don't want to read these, then skip directly to [Installation and configuration](#installing-and-configuring).
Else, Let's try to answer a few questions:-
- what is Django?
- why django?

**What is Django ?**

Django is a framework for web development, written in python

>A framework is built on top of a programming language to aid in a certain type of computer programs, such as a web server or a mobile application. Rocket is a web framework built on top of the Rust programming language.  Rails is a web framework built on top of the Ruby programming language. Django and Flask are two different web frameworks built on top of the Python programming language, each with different pros and cons. React and Angular are front-end web frameworks built on top of the Javascript programming language.[[1]](https://wtfismyengineertalkingabout.com/2017/04/01/whats-the-difference-between-a-programming-language-and-a-framework/)

**Why Django ?**

Before that we can find the answer to why a framework required for development? can we start it from scratch?
>A framework isn’t required to build applications. One could write a web server from scratch in Python without relying on Flask or Django. However, for traditional applications, this would be reinventing the wheel. Frameworks can save engineers a lot of time, and many frameworks have become quite mature, meaning that developers are assured that it is generally safe to use and won’t contain very many bugs – probably a lot fewer than if the developer rewrote the same functionality from scratch. This frees engineers to focus on the code that makes your particular use case unique.[[2]](https://wtfismyengineertalkingabout.com/2017/04/01/whats-the-difference-between-a-programming-language-and-a-framework/)

Ok, now we can answer **why django**?
- Django is popular
- Django have strong developers community base (because of it is popular!)
- When you got stuck some where ! No need to worry we have 100's of helping hands from Django Developers. You can get your answer for your question from telegram channels, stack overflow, articles, etc.(Google too! )
- read more about Django and its features [here](https://steelkiwi.com/blog/why-django-best-web-framework-your-project/)


## Installing and Configuring
### PreReqs
- **Python must be installed** ( We Recommends Python 3) [Use the guide](https://realpython.com/installing-python/)
- **PIP must be configured** [Use the guide](https://www.knowledgehut.com/blog/programming/what-is-pip-in-python)
- **Virtualenv must be installed** [Use the guide](https://www.geeksforgeeks.org/creating-python-virtual-environment-windows-linux/)
- **A simple, modern text editor must be available** (use VSCode/Atom/Sublime ../etc any of these)

### Step 1 :- Create the environment

- create a work directory by using the command by opening a Console/Terminal/Powerhell and enter the commands

    `mkdir mywork`

- change the directory

    `cd mywork`

    ![create workdirectory][wd]

- Create an environment in Windows/Linux/Mac

    `virtualenv -p python3 myenv` or `virtualenv -p python myenv`

    ![create environment][create-env]

- Run the environment by using the command

    `source myenv/bin/activate`

    ![activate environment][act-env]

- You can deactivate the environment by the command ( when ever you want to switch one to another env or once you finish the work)

    `deactivate`

    ![dactivate environment][dact-env]

### Step 2 :- Activate the env, list requirements.txt, install Django

- on your work directory create a file requirements.txt and add the following content to requirements.txt

    `Django`

- activate your env if env not activated by the command, else skip this step

    `source myenv/bin/activate`

- install the requirements by using pip

    `pip install -r requirements.txt`

### Step 3 :- Checking the Django Installation

- activate the environment if it is not activated and run the commands to check django version and Installation

    `python` or `python3`  (this will open the python shell in command-line/Terminal)

    `>>>` `import django`

    `>>>` `print(django.get_version())`

- exit the the shell by entering the command or `CTRL+Z`

    `>>>` `exit()`

### Step 4 :- Creating a Django Project and running with default configuration

- create your first Django Project

    `django-admin startproject myproject`

- check the project configured or not

    `cd myproject`

    `python manage.py makemigrations` or `python3 manage.py makemigrations`

    `python manage.py migrate` or `python3 manage.py migrate`

    `python manage.py runserver` or `python3 manage.py runserver`

- open the  `development server at http://127.0.0.1:8000/` using a web browser

- done

### Create your first django app [continued](#)

### FAQ

- [why we need virtual environment ?](#)
- [what is pip ?](#)
- [why python and python3 in commands ?](#)
- [can I use pycharm like IDE's for this tutorials ?](#)



[wd]: https://gitlab.com/studevsoc/sds-codealong/-/raw/master/django/images/wd.png "Work directory"
[create-env]: https://gitlab.com/studevsoc/sds-codealong/-/raw/master/django/images/create-env.png "create environment"
[act-env]: https://gitlab.com/studevsoc/sds-codealong/-/raw/master/django/images/activate-env.png "activate environment"
[dact-env]: https://gitlab.com/studevsoc/sds-codealong/-/raw/master/django/images/deactivate-env.png "deactivate environment"
